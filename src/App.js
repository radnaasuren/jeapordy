import { ThemeProvider } from '@mui/material';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Host, Game, Home } from './pages/index';
import { customTheme } from './theme';

function App() {
  return (
    <ThemeProvider theme={customTheme}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='host' element={<Host />} />
          <Route path='game' element={<Game />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
