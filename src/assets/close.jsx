import * as React from "react"

export const Close = () => {
    return (
        <svg
            width={24}
            height={24}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g filter="url(#prefix__filter0_d_102_9)">
                <path fill="#fff" d="M4.001 14.948L18.949.001 20 1.052 5.052 16z" />
                <path
                    stroke="#fff"
                    strokeWidth={0.744}
                    d="M4.527 14.948L18.948.527l.526.526L5.053 15.474z"
                />
            </g>
            <g filter="url(#prefix__filter1_d_102_9)">
                <path fill="#fff" d="M18.947 16L4 1.052 5.05.001 20 14.948z" />
                <path
                    stroke="#fff"
                    strokeWidth={0.744}
                    d="M18.948 15.473L4.526 1.052l.527-.526 14.421 14.421z"
                />
            </g>
            <defs>
                <filter
                    id="prefix__filter0_d_102_9"
                    x={0.001}
                    y={0}
                    width={24}
                    height={24}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={4} />
                    <feGaussianBlur stdDeviation={2} />
                    <feComposite in2="hardAlpha" operator="out" />
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                    <feBlend in2="BackgroundImageFix" result="effect1_dropShadow_102_9" />
                    <feBlend
                        in="SourceGraphic"
                        in2="effect1_dropShadow_102_9"
                        result="shape"
                    />
                </filter>
                <filter
                    id="prefix__filter1_d_102_9"
                    x={0}
                    y={0}
                    width={24}
                    height={24}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={4} />
                    <feGaussianBlur stdDeviation={2} />
                    <feComposite in2="hardAlpha" operator="out" />
                    <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                    <feBlend in2="BackgroundImageFix" result="effect1_dropShadow_102_9" />
                    <feBlend
                        in="SourceGraphic"
                        in2="effect1_dropShadow_102_9"
                        result="shape"
                    />
                </filter>
            </defs>
        </svg>
    )
}

