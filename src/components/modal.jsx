import React, { useState } from 'react'
import styled from 'styled-components'
import { Backdrop, Modal, Fade, Typography, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export const QuestionModal = ({ open, question, src, theme, answer, setOpen, points, show, setShow }) => {
    return (
        <Modal
            open={open}
            onClose={() => setOpen(false)}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={open}>
                <Box>
                    <Controls>
                        <Typography variant='h6'>
                            {theme} for {points}
                        </Typography>
                    </Controls>
                    <Typography style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} variant="h2">
                        {question}
                        <div>
                            {show && answer}
                        </div>
                    </Typography>
                    {src && <img src={src} />}
                    <ButtonHolder>
                        {
                            !show &&
                            <Button size='large' variant='outlined' onClick={() => setShow(!show)}>{show ? 'Hide Answer' : 'Show Answer'}</Button>                            
                        }
                        <Button size='large' variant='text' onClick={() => setOpen(false)}>Close</Button>
                    </ButtonHolder>
                </Box>
            </Fade>
        </Modal>
    )
}

export const WinModal = ({ open, setOpen }) => {
    const navigate  = useNavigate()
    const winTitles = ['Winner Winner Chicken dinner', 'VICTORY', 'The most knowledgeable Participant', 'This team not dumb,', '🎉🎉🎉 The winner 🎉🎉🎉', 'You guessed the most points', 'The winner', "Hooray", 'The winer- I mean winner', "The losern't"]

    return (
        <Modal
            open={open}
            onClose={() => setOpen(false)}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={open}>
                <Box>
                    <div />
                    <div>
                        <Typography textAlign='center' variant='h1'>
                            {winTitles[Math.floor(Math.random() * winTitles.length)]}
                        </Typography>
                    </div>
                    <ButtonHolder>
                        <Button size='large' variant='outlined' onClick={() => navigate('/host') }>
                            Back to Set up page
                        </Button>
                    </ButtonHolder>
                </Box>
            </Fade>
        </Modal>
    )
}

const Controls = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    background: #26308b;
`

const Box = styled.div`
    position: absolute; 
    width: 80vw; 
    height: 90vh;
    top: 5vh; 
    left: 10vw; 
    background: #2a3698; 
    border: none;
    outline: none;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: white;
    flex-direction: column;
    transition: all 0.2s;
`

const ButtonHolder = styled.div`
    margin-bottom: 100px;
`