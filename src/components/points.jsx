import { Button, ButtonGroup, TextField } from "@mui/material";
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const TeamPoint = ({ point, setPoint, name, setName }) => {
    const info = {
        'name': 'confo',
        'point': 100,
    }

    class Input extends React.Component {
        _handleKeyDown = (e) => {
            if (e.key === 'Enter') {
                setName(e.target.value);
                console.log(name);
            }
        }
        render() {
            return <TextField value={name} onClick={() => setName()} id="standard-basic" label="Team name" variant="standard" onKeyDown={this._handleKeyDown} ></TextField>
        }
    }

    return (
        <Container>
            <Input></Input>
            <ButtonGroup variant="contained" aria-label="outlined primary button group" style={{ justifyContent: 'space-between' }}>
                <Button onClick={() => setPoint(point - info.point)}>-</Button>
                <StyledPoint>{point}</StyledPoint>
                <Button onClick={() => setPoint(point + info.point)}>+</Button>
            </ButtonGroup>
        </Container>
    )
}

const StyledPoint = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    color:'black';
`;
const StyledName = styled.div`
    display: flex;
    justify-content: center;
`
const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 5px;
`

export { TeamPoint };
