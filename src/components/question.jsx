import React, { useState } from 'react'
import styled from 'styled-components'
import { QuestionModal } from './modal';

export const Question = ({ question, points, src, answer }) => {
    const [open, setOpen] = useState(false)
    const [show, setShow] = useState(false)

    return (
        <div style={{ width: '100%', height: '100%' }}>
            {show ?
                <Quest id='question'>xxx</Quest>
                :
                <Quest id='question' onClick={() => setOpen(true)}>{points}</Quest>
            }

            <QuestionModal answer={answer} points={points} theme='Yomama' setOpen={setOpen} open={open} src={src} question={question} show={show} setShow={setShow} />
        </div>
    )
}

const Quest = styled.div`
    width: 100%;
    height: 100%;
    color : white;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 12px;
    transition: all 0.2s;
    box-sizing: border-box;
    border: 0px solid yellow;
    font-size: 32px;
    font-weight: bold;
    cursor: pointer;
    outline: none;
    :hover {
        filter: brightness(70%);
    }
`
