import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore'
import { useState, useEffect } from 'react';

const firebaseConfig = {
    apiKey: "AIzaSyDLuoWpvszHjtmi2KdFDIX2K6sMEwZUxqM",
    authDomain: "jeapordy-f45ea.firebaseapp.com",
    projectId: "jeapordy-f45ea",
    storageBucket: "jeapordy-f45ea.appspot.com",
    messagingSenderId: "183173618295",
    appId: "1:183173618295:web:d0a7580ec4ff3c7ba04472",
    measurementId: "G-FMMNJ2FB1E"
};

export const useFirebase = () => {
    let [state, setState] = useState({ firebase });
    useEffect(() => {
        let app;
        if (!firebase.apps.length) {
            app = firebase.initializeApp(firebaseConfig);
        }
        let auth = firebase.auth(app)
        let firestore = firebase.firestore(app);
        let rtdb = firebase.database();
        let storage = firebase.storage();
        setState({ firebase, app, auth, firestore, rtdb, storage });
    }, [firebaseConfig]);
    return state;
}
