import React, { useState } from "react";
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { Question } from "../components/question";
import { TeamPoint } from "../components/points";
import { WinModal } from "../components/modal";
import { Button } from "@mui/material";
const Box = styled.div`
    color:white;
    background-color: #2a3698;
    border: 2px solid black;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 32px;
`
const Border = styled.div`
    display: flex;
    border: 2px solid black;
`
export const Game = () => {
    const { state: gameData } = useLocation();
    const [open, setOpen] = useState(false)

    const RenderCategory = ({ Catalog }) => {
        return (
            <div>
                <Box style={{ width: `${90 / gameData.game.length}vw`, height: `${(80 / gameData.game.length)}vh` }}>{Catalog.categoryName}</Box>
                <div>
                    {Object.keys(Catalog.quiz).map((key) => (
                        key <= gameData.config.points &&
                        <Box style={{ padding: '15px' }}><Question answer={Catalog.quiz[key].answer} points={key} question={Catalog.quiz[key].question} /></Box>
                    ))}
                </div>
            </div>
        )
    }

    const RenderTeam = ({team}) => {
        const [point, setPoint] = useState(0)
        const [name, setName] = useState(`team ${team + 1}`) 

        return (
            <Border style={{ margin: '10px', borderRadius: '6px', backgroundColor: 'white' }}>
                <TeamPoint name={name} setName={setName} point={point} setPoint={setPoint} />
            </Border>
        )
    }

    const Teams = () => {
        return (
            <div style={{ width: '100%', height: '150px', display: 'flex', justifyContent: 'center', position: 'absolute', bottom: '50px' }}>
                {
                    Array.from(Array(gameData.config.teams).keys()).map((team) => (<RenderTeam team={team} />))
                }
            </div>
        )
    }
    return (
        <div style={{ height: '100vh', width: '100vw', backgroundColor: '#2a3698', paddingTop: '30px' }}>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                <Border>{gameData.game.map((x) => (<RenderCategory Catalog={x} />))}</Border>
                <Teams />
            </div>
            <WinModal open={open} setOpen={setOpen} />
                <div style={{position:'absolute' , right:'20px' , bottom:'20px'}}>
                    <Button variant='contained' size='large' onClick={() => setOpen(true)}>End Game</Button>
                </div>
        </div>
    )
}