import { Accordion, AccordionDetails, AccordionSummary, Button, Typography } from "@mui/material";
import { styled as style } from '@mui/material/styles';
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const Home = () => {
    return (
        <Container>
            <Typography variant='h1' color='white'>
                JEOPARDY
            </Typography>
            <Link to='/host'>
                <PressPlay variant='contained' size='large'>
                    Set up a game
                </PressPlay>
            </Link>
            <Accordion>
                <AccordionSummary>
                    <Typography>How to play Jeoparty by Press STRT</Typography>
                </AccordionSummary>
                <AccordionDetails style={{ width: 700 }}>
                    <Typography>
                        Go in turns answering questions starting from Team 1. After the team has the answer they will reveal the correct
                        answer and if they are correct they get points, but if they are wrong they lose points(depending on the level of
                        the question indicated by the number, e.g. 100 200 500). But if the team can't answer the question then another
                        team can answer the question and get the points. The winner is the person with the most points at the end. Bonus:
                        even if you only get 3 fourths(75%) of a question correct you still get full points, All teams get 1 question per
                        turn. The game ends after all questions have been revealed
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary>
                    <Typography>How to set up a game</Typography>
                </AccordionSummary>
                <AccordionDetails style={{ width: 700 }}>
                    <Typography>
                        After you press the button above, you will see a "Host" page, where you can select the categories
                        you would like. Then choose the number of teams to play in the game, and finally
                        choose the maximum number of points to be rewarded and press "Create new game".
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary>
                    <Typography>Rules</Typography>
                </AccordionSummary>
                <AccordionDetails style={{ width: 700 }}>
                    <Typography>
                        1. The game ends after all questions have been revealed
                        2. All teams get 1 question per turn.
                        3. If your team cannot answer a question. Another team can answer them on their turn.
                        4. The team with the highest points win.
                        5. You will get full points of the question if you get more than half of the answer correct.
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </Container>
    )
}

const Container = styled.div`
    background: #2a3698;
    height: 100vh;
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    padding-left: 150px;
    padding-top: 200px;
`
const PressPlay = style(Button)({
    padding: '25px',
    paddingLeft: '35px',
    paddingRight: '35px',
    marginTop: '10px',
    fontSize: '16px',
    marginBottom: '30px',
    textDecoration: 'none'
})