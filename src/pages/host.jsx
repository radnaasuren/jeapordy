import React, { useEffect, useState } from "react";
import { FormControl, InputLabel, Select, MenuItem } from '@mui/material'
import styled from 'styled-components'
import { Close } from "../assets/";
import { Button, LoadingButton } from '@mui/material';
import { useNavigate } from "react-router-dom";
import { useFirebase } from '../firebase'
const Border = styled.div`
    display: flex;
    border: 1px solid white;
    border-radius: 8px;
    padding:5px;
`

export const Host = () => {
    const { firebase } = useFirebase();
    const navigate = useNavigate();
    const [selected, setSelected] = useState([])
    const [unSelected, setUnSelected] = useState(['Celebrities', 'Famous Lines', 'History', 'Movies & Books', 'Music', 'Religion', 'Zodiac Signs', 'animals'])
    const [config, setConfig] = useState({
        questions: [],
        teams: 2,
        points: 500,
    })
    const [loading, setLoading] = useState(true)
    const selectedHash = []
    const [gameData, setGameData] = useState([])

    useEffect(() => {
        if (gameData.length == selected.length && selected.length > 0) navigate('/game', { state: { config: config, game: gameData } })
    }, [gameData])

    const removeCategory = (arr, value) => {
        var index = arr.indexOf(value);
        if (index > -1) { arr.splice(index, 1); }
        return arr;
    }
    const SelectedCategorys = () => {
        return (
            <Border>
                <div style={{ height: '100px' }}>
                    {selected?.map((x) => <Button size='large' onClick={() => { setUnSelected([...unSelected, x]); setSelected(removeCategory(selected, x)) }}>{x} X</Button>)}
                </div>
            </Border>
        )
    }
    const UnSelectedCategorys = () => {
        return (
            <Border>
                <div style={{ height: '100px' }}>
                    {unSelected?.map((x) => <Button size='large' onClick={() => { setSelected([...selected, x]); setUnSelected(removeCategory(unSelected, x)) }}>{x}</Button>)}
                </div>
            </Border >
        )
    }
    const HostGame = async () => {
        setGameData([])
        setLoading(true)
        await selected.map((x) => selectedHash[x] = x)
        await firebase.firestore().collection('category').get().then(querySnapshot => {
            querySnapshot.docs.map(async (category) => {
                if (selectedHash[category.id]) {
                    await setGameData((gameData) => [...gameData, { categoryName: category.id, quiz: category.data() }])
                }
            });
        });
    }
    return (
        <div style={{ height: '100vh', width: '100vw', backgroundColor: '#2a3698', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
            <div style={{ fontSize: '48px', marginBottom: '20px', color: 'white' }}>CREATE GAME</div>
            <div style={{ height: '50%', width: '60%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                <SelectedCategorys />
                <UnSelectedCategorys />
                <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ height: '200px', width: '300px' }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Teams</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={config.teams}
                                label="Teams"
                                onChange={(e) => setConfig({ ...config, teams: e.target.value })}
                            >
                                <MenuItem value={1}>One</MenuItem>
                                <MenuItem value={2}>Two</MenuItem>
                                <MenuItem value={3}>Three</MenuItem>
                                <MenuItem value={4}>Four</MenuItem>
                                <MenuItem value={5}>Five</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                    <div style={{ height: '200px', width: '300px' }}>
                        <FormControl fullWidth >
                            <InputLabel id="demo-simple-select-label">Points</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={config.points}
                                label="Points"
                                onChange={(e) => setConfig({ ...config, points: e.target.value })}
                            >
                                <MenuItem value={500}>500</MenuItem>
                                <MenuItem value={600}>600</MenuItem>
                                <MenuItem value={700}>700</MenuItem>
                                <MenuItem value={800}>800</MenuItem>
                                <MenuItem value={900}>900</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                </div>
                <Button variant="contained" style={{ color: selected.length >= 5 && '#2a3698'}} disabled={selected.length >= 5 ? false : true} onClick={() => { HostGame() }}>CREATE NEW GAME</Button>
            </div>
        </div >
    )
}