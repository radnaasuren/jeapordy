import { createTheme } from '@mui/material/styles';

export const customTheme = createTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#ffffff',
        },
        background: {
            default: '#2a3698',
        },
    },
});